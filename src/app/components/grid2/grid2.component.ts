import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ImageLibraryService } from 'src/app/services/imageLibrary'

@Component({
  selector: 'app-grid2',
  templateUrl: './grid2.component.html',
  styleUrls: ['./grid2.component.css']
})

export class Grid2Component implements OnInit {

  size!: number;
  
  enablePrev :boolean = false;
  enableNext :boolean = true;

  selected : number = 0;
  urls: string[];

  constructor(private library: ImageLibraryService) {
    this.urls = library.getUrls();
   }

  isSelected(i : number) : boolean {
    if (i == this.selected) return true;
    else return false;
  }

  updateStatus() {
    this.enablePrev = this.library.getCurrentIndex() == 0 ? false : true;
    this.enableNext = this.library.getCurrentIndex() == this.library.getSize() - 1  ? false : true;
    this.selected = this.library.getCurrentIndex();
  }

  select(index : number): void {
    this.library.setCurrentIndex(index);
  }

  ngOnInit(): void {
     this.library.currentImageObserver.subscribe( (next :number) => this.updateStatus());
     this.library.currentZoomObserver.subscribe( (next : number) => { this.size = this.library.getCurrentImageScaleFactor(); console.log(this.size); });
  }

  prev() {
    this.library.moveToPrevImage();
  }

  next() {
    this.library.moveToNextImage();
  }

  first()  {
    this.library.setCurrentIndex(0);
  }

  last() {
    this.library.setCurrentIndex(this.library.getSize()-1);
  }

  zoom(event : any) {
    this.library.setCurrentScaleFactor(event.srcElement.value);
  }

}
