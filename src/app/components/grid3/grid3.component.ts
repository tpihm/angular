import { Component, OnInit, Input } from '@angular/core';
import { ImageLibraryService } from 'src/app/services/imageLibrary'

@Component({
  selector: 'app-grid3',
  templateUrl: './grid3.component.html',
  styleUrls: ['./grid3.component.css']
})
export class Grid3Component implements OnInit {

  constructor(private library: ImageLibraryService) { 
    this.url = this.library.getCurrentImageUrl();
    this.size = this.library.getCurrentImageScaleFactor();
  }

  updateImage() {
    this.url = this.library.getCurrentImageUrl();
    this.size = this.library.getCurrentImageScaleFactor();

  }
  url! : string;
  size! : number;

  ngOnInit(): void {
    this.library.currentImageObserver.subscribe( () => this.updateImage());
    this.library.currentZoomObserver.subscribe( () => this.updateImage());
  }

}
