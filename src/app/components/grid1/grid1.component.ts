import { Component } from '@angular/core';
import { Timer } from './timer';
@Component({
  selector: 'app-grid1',
  templateUrl: './grid1.component.html',
  styleUrls: ['./grid1.component.css']
})

export class Grid1Component  {

  enablePlay: boolean = true;
  enableStop: boolean = false;
  enableBackward: boolean = false;
  timer: Timer = new Timer();
  _btnPlay: string = 'Démarrer';


  play() {
    this.timer.start();
    this._btnPlay = 'Continuer';

    this.enableStop = true;
    this.enablePlay = this.enableBackward = false;
  
  }

  stop() {
    this.timer.stop();
  
    this.enableStop = false;
    this.enablePlay = this.enableBackward = true;
  }
  
  backward() {
    this.timer.reset();
    this._btnPlay = 'Démarrer';

    this.enablePlay = true;
    this.enableStop = this.enableBackward = false;
  
  }

}
