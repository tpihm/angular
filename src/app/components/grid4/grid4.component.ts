import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid4',
  templateUrl: './grid4.component.svg',
  styleUrls: ['./grid4.component.css']
})
export class Grid4Component implements OnInit {

  fillColor = 'rgb(255, 0, 0)';
 
  angle : number = 1;

  changeColor() {
    const r = Math.floor(Math.random() * 256);
    const g = Math.floor(Math.random() * 256);
    const b = Math.floor(Math.random() * 256);
    this.fillColor = `rgb(${r}, ${g}, ${b})`;
  }

  constructor() { 
   
  }

  ngOnInit(): void {
  }

  record(event : MouseEvent) {
    console.log(event.x +" " + event.y + " - ");
  }

   rotate() : string {
    return "rotate(20)";
   }
 
}
