import { ReadKeyExpr } from '@angular/compiler';
import { Component, Output } from '@angular/core';
import { ImageLibraryService } from './services/imageLibrary';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ImageLibraryService]
})

export class AppComponent {
  title = 'dashbord-project';

  

  constructor(private library: ImageLibraryService) {
    
  }


  ngOnInit(): void {}

  
}
