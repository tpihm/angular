import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs';

type imageItem = {
  url: string ;
  size: number;
};

@Injectable({
  providedIn: 'root'
})

export class ImageLibraryService {

  private images: string[] = ["un.png", "geste.png", "pour.png",
	"la.png", "pla.png", "ne.png", "te.png"];

  getSize() {
    return this.library.length;
  }

	public getCurrentIndex(): number {
		return this.currentIndexObserver.getValue();
	}

	public moveToNextImage(): number {
		this.setCurrentIndex(this.currentIndexObserver.getValue() + 1);
		return this.currentIndexObserver.getValue();
	}

	public moveToPrevImage(): number {
		this.setCurrentIndex(this.currentIndexObserver.getValue() - 1);
		return this.currentIndexObserver.getValue();
	}

	public getCurrentImageUrl(): string {
		return this.library[this.currentIndexObserver.getValue()].url;
	}

	public getCurrentImageScaleFactor(): number {
		return this.library[this.currentIndexObserver.getValue()].size;
	}

	public setCurrentIndex(nouvelIndex: number) {
		if ((nouvelIndex < this.library.length) && (nouvelIndex >= 0)) {
			this.currentIndexObserver.next(nouvelIndex);
			this.currentScaleFactorObserver.next(this.getCurrentImageScaleFactor());
		}
	}

	public setCurrentScaleFactor(factor: number) {
		this.library[this.currentIndexObserver.getValue()].size = factor;
		this.currentScaleFactorObserver.next(factor);
	}

  public getUrls() : string[] {
    return this.library.map( (x) => x.url );
  }

	public constructor() {
		this.library = new Array<imageItem>();
		for (let i = 0; i < this.images.length; i++)
      this.library.push({ url : "../assets/images/" + this.images[i], size : 300});

	}

  private library: Array<imageItem>; // bibliotheque d image
  private currentIndexObserver = new BehaviorSubject<number>(0);
  public currentImageObserver = this.currentIndexObserver.asObservable();

  private currentScaleFactorObserver = new BehaviorSubject<number>(0);
  public currentZoomObserver = this.currentScaleFactorObserver.asObservable();
}

